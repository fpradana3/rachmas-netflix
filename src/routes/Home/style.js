import styled from '@emotion/styled';
import logoprofil from '../../assets/logoprofile.png'
import logokids from '../../assets/logokids.png'
import logoadd from '../../assets/round-add-button.png'

export const Content = styled.div`
    @media screen {

        font-family: 'Netflix Sans','Helvetica Neue',Helvetica,Arial,sans-serif;

        background-color: #141414;
        height: 100%;
        min-height:100vh;
        
        .logonetflixcontainer{
            width:100%;
        }

        .logonetflix{
            margin-left:4vw;
            cursor:pointer;
            margin-top:0.9vh;
            height:calc(3vw + 2vh);
        }

        .kontentengah{
            position: absolute;
            width: 85%;
            top: 50%;
            transform: translate(-50%, -50%);
            left: 50%;
        }

        .tulisanhome{
            text-align:center;
            font-size:calc(20px + 2vw);
            font-weight:500;
            margin-top:0!important;
            color: #fff;
            margin:4vh;
            font-family: 'Netflix Sans','Helvetica Neue',Helvetica,Arial,sans-serif;
        }

        .kontainerkonten{
            display:flex;
            flex-direction: column;
        }

        .kontainerprofil{
            display:flex;
            width:min-content;
            margin-right:auto;
            margin-left:auto;
        }

        .myprofile{
            background-image: url(${logoprofil});
            background-size: 100%;
            border-radius:4px;
            margin-left:auto;
            margin-right:auto;
            height:calc(70px + 5vw);
            width:calc(70px + 5vw);
            cursor:pointer;
        }

        .kidsprofile{
            background-image: url(${logokids});
            background-size: 100%;
            border-radius:4px;
            margin-left:auto;
            margin-right:auto;
            height:calc(70px + 5vw);
            width:calc(70px + 5vw);
            cursor:pointer;
        }

        .addprofile{
            background-image: url(${logoadd});
            background-size: 60%;
            background-repeat: no-repeat;
            background-position: center center;
            border-radius:4px;
            margin-left:auto;
            margin-right:auto;
            height:calc(70px + 5vw);
            width:calc(70px + 5vw);
            cursor:pointer;
        }

        .myprofilecontainer:hover .addprofile{
            background-color:white;
        }

        .myprofilecontainer{
            margin-left:auto;
            margin-right:auto;
            padding-left:1.2vw;
            padding-right:1.2vw;
            display:flex;
            flex-direction: column;
            text-decoration: none;
        }

        .myprofilecontainer:hover .namaprofile{
            color:#fff;
        }

        .myprofilecontainer:hover .myprofile{
            outline: white solid 4px;
        }

        .myprofilecontainer:hover .kidsprofile{
            outline: white solid 4px;
        }

        .namaprofile{
            font-size:1.5vw;
            text-align:center;
            margin:1vh;
            color: grey;
            cursor: pointer;
        }

        @media (max-width: 1000px) {
            background-color:black;

            .namaprofile {
                font-size: 16px;
            }
        }
    }

        
            
        `;
