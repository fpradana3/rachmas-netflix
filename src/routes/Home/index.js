import React from 'react';
import '../../index.css';
import { Content } from './style';
import { Link } from 'react-router-dom';
import logonetflix from '../../assets/logonetflix.png'



const Home = () => {
  

    return (
        <Content>
            <div class="logonetflixcontainer">
              <Link to="/">
                <img class="logonetflix" src={logonetflix} alt="logo netflix"></img>
              </Link>
            </div>

            <div class="kontentengah">
              <div class="kontainerkonten">
                <p class="tulisanhome">Who's watching?</p>
                <div class="kontainerprofil">
                  <Link class="myprofilecontainer" to="/browse">
                    <div class="myprofile"></div>
                    <p class="namaprofile">Rachma</p>
                  </Link>


                  <div class="myprofilecontainer">
                    <div class="kidsprofile"></div>
                    <p class="namaprofile">Children</p>
                  </div>    

                  <div class="myprofilecontainer">
                    <div class="addprofile"></div>
                    <p class="namaprofile">Add Profile</p>
                  </div>   
                </div>



                
              </div>
            </div>
        </Content>
      );
};

export default Home;
