import React, {useEffect} from 'react';
import '../../../index.css';
import { Content } from './style';
import BigInnerNavbar from '../../../components/BigInnerNavbar';
import YouTube from 'react-youtube';
import InnerNavbar from '../../../components/InnerNavbar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faPlay, faDownload, faShareNodes} from '@fortawesome/free-solid-svg-icons';



const RachmaMovie = () => {

    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])

    const opts = {
        height:"100%",
        width: '100%',
      };
    
    const [width, setWidth] = React.useState(window.innerWidth);
    const breakpoint = 450;
    React.useEffect(() => {
    const handleResizeWindow = () => setWidth(window.innerWidth);
        window.addEventListener("resize", handleResizeWindow);
        return () => {
        window.removeEventListener("resize", handleResizeWindow);
        };
    }, []);
    if (width > breakpoint) {
        return (
        <Content>
            <BigInnerNavbar/>
            <div class="atas"></div>
            <YouTube containerClassName="youtubeatas" opts={opts} videoId="4m83Hs7K0TQ" />
            <div class="isi">
                <p class="judulrachma">Happy Birthday: Thank You for being Born, Rachma</p>
                <p class="tahun">2022</p>
                <div class="kontainerplaydown">
                    <div class="kontainertomplay">
                        <FontAwesomeIcon class="iconplay" icon={faPlay} />
                        <p class="tulisantomplay">Play</p>
                    </div>
                    <div class="kontainertomdown">
                        <FontAwesomeIcon class="iconplay2" icon={faDownload} />
                        <p class="tulisantomdown">Download</p>
                    </div>    
                </div>

                <div class="kontainerdesc">
                    <p class="wish">29 Februari, tanggal yang sebenarnya tidak ada pada tahun ini, tapi terlalu berharga untuk dilewatkan. Sayangnya, aku menulis ini sambil berharap untuk dapat mengatakan semua ini langsung dan berada di sampingmu saat itu tiba. Pasti sangat menyenangkan jika aku bisa menghabiskan hari itu bersamamu. Tapi tidak apa, aku yakin masih banyak 29 februari lainnya yang akan kita lalui bersama-sama. Karena itu, aku ingin berkata: Hai, selamat ulang tahun, Rachma Hermawan!</p>
                    <p class="starring">Starring: Chacha, Soya, Rena, Yuni, Cici, Sefia, Dana</p>
                </div>

                <div class="kontainerIcon">
                    <div class="kontainermylist">
                        <FontAwesomeIcon class="icon2" icon={faPlus} />
                        <p class="tulisanmylist">My List</p>
                    </div>

                    <div class="kontainermylist">
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon2" viewBox="0 0 512 512"><path d="M96 191.1H32c-17.67 0-32 14.33-32 31.1v223.1c0 17.67 14.33 31.1 32 31.1h64c17.67 0 32-14.33 32-31.1V223.1C128 206.3 113.7 191.1 96 191.1zM512 227c0-36.89-30.05-66.92-66.97-66.92h-99.86C354.7 135.1 360 113.5 360 100.8c0-33.8-26.2-68.78-70.06-68.78c-46.61 0-59.36 32.44-69.61 58.5c-31.66 80.5-60.33 66.39-60.33 93.47c0 12.84 10.36 23.99 24.02 23.99c5.256 0 10.55-1.721 14.97-5.26c76.76-61.37 57.97-122.7 90.95-122.7c16.08 0 22.06 12.75 22.06 20.79c0 7.404-7.594 39.55-25.55 71.59c-2.046 3.646-3.066 7.686-3.066 11.72c0 13.92 11.43 23.1 24 23.1h137.6C455.5 208.1 464 216.6 464 227c0 9.809-7.766 18.03-17.67 18.71c-12.66 .8593-22.36 11.4-22.36 23.94c0 15.47 11.39 15.95 11.39 28.91c0 25.37-35.03 12.34-35.03 42.15c0 11.22 6.392 13.03 6.392 22.25c0 22.66-29.77 13.76-29.77 40.64c0 4.515 1.11 5.961 1.11 9.456c0 10.45-8.516 18.95-18.97 18.95h-52.53c-25.62 0-51.02-8.466-71.5-23.81l-36.66-27.51c-4.315-3.245-9.37-4.811-14.38-4.811c-13.85 0-24.03 11.38-24.03 24.04c0 7.287 3.312 14.42 9.596 19.13l36.67 27.52C235 468.1 270.6 480 306.6 480h52.53c35.33 0 64.36-27.49 66.8-62.2c17.77-12.23 28.83-32.51 28.83-54.83c0-3.046-.2187-6.107-.6406-9.122c17.84-12.15 29.28-32.58 29.28-55.28c0-5.311-.6406-10.54-1.875-15.64C499.9 270.1 512 250.2 512 227z" fill='currentColor'/></svg>
                        <p class="tulisanmylist">Rate</p>
                    </div>

                    <div class="kontainermylist">
                        <FontAwesomeIcon class="icon2" icon={faShareNodes} />
                        <p class="tulisanmylist">Share</p>
                    </div>

                    <div class="kontainermylist">
                        <FontAwesomeIcon class="icon2" icon={faDownload} />
                        <p class="tulisanmylist">Download</p>
                    </div>
                </div>



                <div class="kontainerlistepisode">

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode" opts={opts} videoId="0szrWEA-eOQ" />
                            <div class="kontainertulisanepisode">
                                <p class="tulisanepisode1">1. Episode 1: Chacha</p>
                                <p class="tulisanepisode2">14m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Chacha. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode" opts={opts} videoId="h0cHXYUeGcg" />
                            <div class="kontainertulisanepisode">
                                <p class="tulisanepisode1">2. Episode 2: Soya</p>
                                <p class="tulisanepisode2">5m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Soya. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode" opts={opts} videoId="sMNrv2pxbTM" />
                            <div class="kontainertulisanepisode">
                                <p class="tulisanepisode1">3. Episode 3: Rena</p>
                                <p class="tulisanepisode2">5m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Rena. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode" opts={opts} videoId="46k-nhBcbj0" />
                            <div class="kontainertulisanepisode">
                                <p class="tulisanepisode1">4. Episode 4: Yuni</p>
                                <p class="tulisanepisode2">10m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Yuni. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode" opts={opts} videoId="LFYj7vqBFVE" />
                            <div class="kontainertulisanepisode">
                                <p class="tulisanepisode1">5. Episode 5: Cici</p>
                                <p class="tulisanepisode2">14m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Cici. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode" opts={opts} videoId="8DpixZKRR2g" />
                            <div class="kontainertulisanepisode">
                                <p class="tulisanepisode1">6. Episode 6: Sefia</p>
                                <p class="tulisanepisode2">4m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Sefia. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode" opts={opts} videoId="FLpLOYy61XQ" />
                            <div class="kontainertulisanepisode">
                                <p class="tulisanepisode1">7. Episode 7: Paul</p>
                                <p class="tulisanepisode2">1m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Paul. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode" opts={opts} videoId="qwqLpbjz_Jo" />
                            <div class="kontainertulisanepisode">
                                <p class="tulisanepisode1">12. Episode 12: Dana</p>
                                <p class="tulisanepisode2">15m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Selamat ulang tahun yaa! Semoga semoga semogaa! Aku sayang kamu selamanya, have a beautiful day sayang.</p>
                    </div>


                </div>
            </div>
        </Content>
        );
    }

    return (
        <Content>
            <InnerNavbar/>
            <div class="atas"></div>
            <YouTube containerClassName="youtubeatasmobile" opts={opts} videoId="4m83Hs7K0TQ" />

            <div class="isi">
                <p class="judulrachma">Happy Birthday: Thank You for being Born, Rachma</p>
                <p class="tahun">2022</p>
                <div class="kontainerplaydown">
                    <div class="kontainertomplay">
                        <FontAwesomeIcon class="iconplay" icon={faPlay} />
                        <p class="tulisantomplay">Play</p>
                    </div>
                    <div class="kontainertomdown">
                        <FontAwesomeIcon class="iconplay2" icon={faDownload} />
                        <p class="tulisantomdown">Download</p>
                    </div>
                </div>

                <div class="kontainerdesc">
                    <p class="wish">29 Februari, tanggal yang sebenarnya tidak ada pada tahun ini, tapi terlalu berharga untuk dilewatkan. Sayangnya, aku menulis ini sambil berharap untuk dapat mengatakan semua ini langsung dan berada di sampingmu saat itu tiba. Pasti sangat menyenangkan jika aku bisa menghabiskan hari itu bersamamu. Tapi tidak apa, aku yakin masih banyak 29 februari lainnya yang akan kita lalui bersama-sama. Karena itu, aku ingin berkata: Hai, selamat ulang tahun, Rachma Hermawan!</p>
                    <p class="starring">Starring: Chacha, Soya, Rena, Yuni, Cici, Sefia, Dana</p>
                </div>

                <div class="kontainerIcon">
                    <div class="kontainermylist">
                        <FontAwesomeIcon class="icon2" icon={faPlus} />
                        <p class="tulisanmylist">My List</p>
                    </div>

                    <div class="kontainermylist">
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon2" viewBox="0 0 512 512"><path d="M96 191.1H32c-17.67 0-32 14.33-32 31.1v223.1c0 17.67 14.33 31.1 32 31.1h64c17.67 0 32-14.33 32-31.1V223.1C128 206.3 113.7 191.1 96 191.1zM512 227c0-36.89-30.05-66.92-66.97-66.92h-99.86C354.7 135.1 360 113.5 360 100.8c0-33.8-26.2-68.78-70.06-68.78c-46.61 0-59.36 32.44-69.61 58.5c-31.66 80.5-60.33 66.39-60.33 93.47c0 12.84 10.36 23.99 24.02 23.99c5.256 0 10.55-1.721 14.97-5.26c76.76-61.37 57.97-122.7 90.95-122.7c16.08 0 22.06 12.75 22.06 20.79c0 7.404-7.594 39.55-25.55 71.59c-2.046 3.646-3.066 7.686-3.066 11.72c0 13.92 11.43 23.1 24 23.1h137.6C455.5 208.1 464 216.6 464 227c0 9.809-7.766 18.03-17.67 18.71c-12.66 .8593-22.36 11.4-22.36 23.94c0 15.47 11.39 15.95 11.39 28.91c0 25.37-35.03 12.34-35.03 42.15c0 11.22 6.392 13.03 6.392 22.25c0 22.66-29.77 13.76-29.77 40.64c0 4.515 1.11 5.961 1.11 9.456c0 10.45-8.516 18.95-18.97 18.95h-52.53c-25.62 0-51.02-8.466-71.5-23.81l-36.66-27.51c-4.315-3.245-9.37-4.811-14.38-4.811c-13.85 0-24.03 11.38-24.03 24.04c0 7.287 3.312 14.42 9.596 19.13l36.67 27.52C235 468.1 270.6 480 306.6 480h52.53c35.33 0 64.36-27.49 66.8-62.2c17.77-12.23 28.83-32.51 28.83-54.83c0-3.046-.2187-6.107-.6406-9.122c17.84-12.15 29.28-32.58 29.28-55.28c0-5.311-.6406-10.54-1.875-15.64C499.9 270.1 512 250.2 512 227z" fill='currentColor'/></svg>
                        <p class="tulisanmylist">Rate</p>
                    </div>

                    <div class="kontainermylist">
                        <FontAwesomeIcon class="icon2" icon={faShareNodes} />
                        <p class="tulisanmylist">Share</p>
                    </div>

                    <div class="kontainermylist">
                        <FontAwesomeIcon class="icon2" icon={faDownload} />
                        <p class="tulisanmylist">Download</p>
                    </div>
                </div>

                <div class="kontainerlistepisode">
                    
                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode2" opts={opts} videoId="0szrWEA-eOQ" />
                            <div class="kontainertulisanepisode2">
                                <p class="tulisanepisode1">1. Episode 1: Chacha</p>
                                <p class="tulisanepisode2">14m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Chacha. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode2" opts={opts} videoId="h0cHXYUeGcg" />
                            <div class="kontainertulisanepisode2">
                                <p class="tulisanepisode1">2. Episode 2: Soya</p>
                                <p class="tulisanepisode2">5m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Soya. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode2" opts={opts} videoId="sMNrv2pxbTM" />
                            <div class="kontainertulisanepisode2">
                                <p class="tulisanepisode1">3. Episode 3: Rena</p>
                                <p class="tulisanepisode2">5m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Rena. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode2" opts={opts} videoId="46k-nhBcbj0" />
                            <div class="kontainertulisanepisode2">
                                <p class="tulisanepisode1">4. Episode 4: Yuni</p>
                                <p class="tulisanepisode2">10m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Yuni. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode2" opts={opts} videoId="LFYj7vqBFVE" />
                            <div class="kontainertulisanepisode2">
                                <p class="tulisanepisode1">5. Episode 5: Cici</p>
                                <p class="tulisanepisode2">14m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Cici. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode2" opts={opts} videoId="8DpixZKRR2g" />
                            <div class="kontainertulisanepisode2">
                                <p class="tulisanepisode1">6. Episode 6: Sefia</p>
                                <p class="tulisanepisode2">4m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Sefia. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode2" opts={opts} videoId="FLpLOYy61XQ" />
                            <div class="kontainertulisanepisode2">
                                <p class="tulisanepisode1">7. Episode 7: Paul</p>
                                <p class="tulisanepisode2">4m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Berikut adalah video ucapan selamat ulang tahun dari Paul. Semoga suka yaa, Happy Birthday!</p>
                    </div>

                    <div class="kontainerepisode">
                        <div class="episodeatas">
                            <YouTube containerClassName="youtubeepisode2" opts={opts} videoId="qwqLpbjz_Jo" />
                            <div class="kontainertulisanepisode2">
                                <p class="tulisanepisode1">12. Episode 12: Dana</p>
                                <p class="tulisanepisode2">15m</p>
                            </div>
                            <div class="kontainericonepisode">
                                <FontAwesomeIcon class="iconepisode" icon={faDownload} />
                            </div>        
                        </div>
                        <p class="episodebawah">Halo Rachma, Selamat ulang tahun yaa! Semoga semoga semogaa! Aku sayang kamu selamanya, have a beautiful day sayang.</p>
                    </div>

                </div>
            </div>
        </Content>
      );
};

export default RachmaMovie;
