import styled from '@emotion/styled';

export const Content = styled.div`
    @media screen {

        font-family: 'Netflix Sans','Helvetica Neue',Helvetica,Arial,sans-serif;

        .kontainerepisode{
            width:100%;
            display:flex;
            flex-direction:column;
            margin-bottom:4vh;
        }

        .episodeatas{
            width:95%;
            display:flex;
            margin-left:auto;
            margin-right:auto;
        }

        .episodebawah{
            width:95%;
            margin-left:auto;
            margin-right:auto;
            color:whitesmoke;
            text-align:justify;
            font-size:calc(1vw + 1vh);
            line-height:140%;
        }

        .youtubeepisode{
            flex:35%;
            height:35vh;
        }

        .youtubeepisode2{
            flex:40%;
            height:13vh;
        }

        .kontainertulisanepisode{
            padding-left:4vw;
            flex:55%;
            display:flex;
            justify-content:center;
            flex-direction:column;
        }

        .kontainertulisanepisode2{
            padding-left:4vw;
            flex:50%;
            display:flex;
            justify-content:center;
            flex-direction:column;
        }

        .tulisanepisode1{
            color:whitesmoke;
            font-weight:700;
            font-size:calc(1.2vw + 1.2vh);
            margin-bottom:0;
        }

        .tulisanepisode2{
            color:grey;
            font-size:calc(1vw + 1vh);
        }

        .isi{
            width:95%;
            margin-left:auto;
            margin-right:auto;
        }

        .kontainerIcon{
            width:95%;
            display:flex;
            margin-top:calc(2vh + 2vw);
            margin-bottom:calc(1.5vh + 1.5vw);
            margin-right:auto;
            margin-left:auto;
        }

        .kontainermylist{
            flex:25%;
            display:flex;
            flex-direction:column;
            font-size:calc(4.5px + 1vw);
            text-align:center;
            cursor:pointer;
        }

        .icon2{
            color:whitesmoke;
            height:calc(10px + 3vw)!important;
        }
        
        .kontainericonepisode{
            flex:10%;
            display:flex;
            justify-content:center;
            align-items:center;
        }

        .iconepisode{
            color:whitesmoke;
            height:calc(10px + 3vw)!important;
        }


        .kontainertomplay{
            display:flex;
            padding:10px;
            background-color:whitesmoke;
            border-radius:4px;
            align-items:center;
            justify-content: center;
            cursor:pointer;
            margin-bottom:1vh;
        }

        .kontainertomdown{
            display:flex;
            padding:10px;
            background-color:#141414;;
            border-radius:4px;
            align-items:center;
            justify-content: center;
            cursor:pointer;
            margin-bottom:1vh;
        }

        .kontainertomplay:hover{
            background-color: rgb(109,109,109);
        }

        .kontainertomdown:hover{
            background-color: #1b1b1b;
        }

        .iconplay{
            height:calc(8px + 2vw)!important;
            color:black;
            margin-left:calc(4px + 0.7vw);
            margin-right:calc(4px + 1.2vw);
        }

        .iconplay2{
            height:calc(4px + 2vw)!important;
            color:whitesmoke;
            margin-left:calc(4px + 0.7vw);
            margin-right:calc(4px + 1.2vw);
        }

        .tulisantomplay{
            color:black;
            font-size:calc(4px + 2vw);
            margin-right:calc(4px + 0.7vw);
            font-weight:700;
            margin-top:0.5vh;
            margin-bottom:0.5vh;
        }

        .tulisantomdown{
            color:whitesmoke;
            font-size:calc(4px + 2vw);
            margin-right:calc(4px + 0.7vw);
            font-weight:700;
            margin-top:0.5vh;
            margin-bottom:0.5vh;
        }

        .judulrachma{
            color:whitesmoke;
            font-size:calc(1.8vw + 1.8vh);
            font-weight:600;
            margin-top:3vh;
            margin-bottom:2vh;
        }

        .tahun{
            color:grey;
            font-size:calc(1vw + 1vh);
            margin-top:1.5vh;
        }



        background-color: black;
        height: 100%;
        min-height:150vh;
        color:white;

        .atas{
            width:100%;
            height:10vh;
        }

        .youtubeatas{
            margin-left:auto;
            margin-right:auto;
            width:95vw;
            height:70vh;
        }

        .youtubeatasmobile{
            margin-left:auto;
            margin-right:auto;
            width:95vw;
            height:30vh;
        }

        .kontainerlistepisode{
            border-top: 0.4vh solid #141414;
            padding-top:5vh;
            padding-bottom:20vh;
        }

        .wish{
            color:whitesmoke;
            text-align:justify;
            font-size:calc(1vw + 1vh);
            line-height:140%;
        }

        .starring{
            color:grey;
            font-size:calc(1vw + 1vh);
        }

        @media (min-width: 1000px) {
            background-color: #141414;

            .kontainerplaydown{
                display:flex;
            }

            .kontainertomdown{
                background-color:#4d4d4d;
                flex:50%;
                margin-left:0.5vw;
            }

            .kontainerlistepisode{
                border-top: 0.4vh solid #4d4d4d;
            }

            .kontainertomplay{
                flex:50%;
                margin-right:0.5vw;
            }
        }

        

      
    }

        
            
        `;
