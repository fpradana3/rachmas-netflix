import styled from '@emotion/styled';
import bannerrachma from '../../../../assets/rachma.jpg'

export const Content = styled.div`
    @media screen {
        font-family: 'Netflix Sans','Helvetica Neue',Helvetica,Arial,sans-serif;
        background-color: #141414;
        height: 100%;
        min-height:100vh;


        .top10{
            width:100%;
            display:flex;
            overflow-x:auto;
            overflow-y:hidden;
        }

        .footer{
            height:10vh;
        }

        .top10::-webkit-scrollbar{
            width:0;
            height:0;
        }

        .judulkategori{
            color:whitesmoke;
            margin-left:4vw;
            font-weight:600;
            font-size:calc(1.5vw + 1.5vh);
            margin-top:1vh;
            margin-bottom:2vh;
        }
        
        .judulkategori2{
            color:whitesmoke;
            margin-left:4vw;
            font-weight:600;
            font-size:calc(1.5vw + 1.5vh);
            margin-top:4vh;
            margin-bottom:2vh;
        }









        .kontainerkonten{
            display:flex;
            flex-direction:column;
            position:absolute;
            top: 78%;
            transform: translate(-50%, -50%);
            left: 50%;
            color:whitesmoke;
            width:90%
        }

        .kontainergenre{
            width:100%;
            display:flex;
            text-align:center;
            justify-content:center;
            align-items:center;
            color:whitesmoke;
            margin-bottom:2vh;
        }

        .tag{
            color:grey;
        }

        .genretag{
            font-size:calc(4px + 1.5vw);
            margin-left:calc(4px + 3vw);
            margin-right:calc(4px + 3vw);
        }

        .kontainertombol{
            width:100%;
            display:flex;
            
        }

        .kontainerplay{
            display:flex;
            flex:33.3%;
            align-items:center;
            text-decoration: none;
            justify-content: center;
        }

        .kontainertombolplay{
            display:flex;
            padding:10px;
            background-color:whitesmoke;
            border-radius:4px;
            align-items:center;
            justify-content: center;
        }

        .kontainertombolplay:hover{
            background-color: rgb(109,109,109);
        }

        .kontainermylist{
            flex:33.3%;
            display:flex;
            flex-direction:column;
            font-size:calc(4.5px + 1vw);
            text-align:center;
            cursor:pointer;
        }

        .icon{
            height:calc(10px + 3vw)!important;
        }

        .iconplay{
            height:calc(8px + 2vw)!important;
            color:black;
            margin-left:calc(4px + 0.7vw);
            margin-right:calc(4px + 1.2vw);
        }

        .tulisanplay{
            color:black;
            font-size:calc(4px + 2vw);
            margin-right:calc(4px + 0.7vw);
            font-weight:700;
            margin-top:0.5vh;
            margin-bottom:0.5vh;
        }

        .tulisanmylist{
            margin-top:1vh;
            margin-bottom:0;
        }


        
        .logonetflixcontainer{
            position:fixed;
            
            width:100%;
        }

        .logonetflix{
            margin-left:4vw;
            cursor:pointer;
            margin-top:2vh;
            height:calc(3vw + 2vh);
        }

        .kontentengah{
            width:100%;
            height:100vh;
        }

        .banner{
            min-height:100vh;
            background-repeat: no-repeat;
            background-image:linear-gradient(transparent 30%, #141414), url(${bannerrachma});
            background-position:center;
            background-size:cover;
        }

        @media (max-width: 700px) {
            .kontainerkonten{
                top:90%;
            }
        }

        @media (max-width: 1000px) {
            background-color:black;



            .banner{
                background-image:linear-gradient(transparent 30%, black), url(${bannerrachma});
            }
        }

    }

        
            
        `;
