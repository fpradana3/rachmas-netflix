import React, {useState, useEffect} from 'react';
import '../../../../index.css';
import { Content } from './style';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faPlay } from '@fortawesome/free-solid-svg-icons';
import iconinfo from '../../../../assets/iconinfo.svg'
import top1 from '../../../../assets/top1.jpg'
import top2 from '../../../../assets/top2.jpg'
import top3 from '../../../../assets/top3.jpg'
import top4 from '../../../../assets/top4.jpg'
import top5 from '../../../../assets/top5.jpg'
import top6 from '../../../../assets/top6.jpg'
import top7 from '../../../../assets/top7.jpg'
import top8 from '../../../../assets/top8.jpg'
import top9 from '../../../../assets/top9.jpg'
import top10 from '../../../../assets/top10.jpg'
import Top10Card from '../../../../components/Cards/Top10';
import tren1 from '../../../../assets/tren1.jpg'
import tren2 from '../../../../assets/tren2.jpg'
import tren3 from '../../../../assets/tren3.jpg'
import tren4 from '../../../../assets/tren4.jpg'
import tren5 from '../../../../assets/tren5.jpg'
import tren6 from '../../../../assets/tren6.jpg'
import tren7 from '../../../../assets/tren7.jpg'
import tren8 from '../../../../assets/tren8.jpg'
import tren9 from '../../../../assets/tren9.jpg'
import tren10 from '../../../../assets/tren10.jpg'
import NormalCard from '../../../../components/Cards/NormalCard';
import only1 from '../../../../assets/only1.jpg'
import only2 from '../../../../assets/only2.jpg'
import only3 from '../../../../assets/only3.jpg'
import only4 from '../../../../assets/only4.jpg'
import only5 from '../../../../assets/only5.jpg'
import only6 from '../../../../assets/only6.jpg'
import only7 from '../../../../assets/only7.jpg'
import only8 from '../../../../assets/only8.jpg'
import only9 from '../../../../assets/only9.jpg'
import only10 from '../../../../assets/only10.jpg'
import west1 from '../../../../assets/west1.jpg'
import west2 from '../../../../assets/west2.jpg'
import west3 from '../../../../assets/west3.jpg'
import west4 from '../../../../assets/west4.jpg'
import west5 from '../../../../assets/west5.jpg'
import west6 from '../../../../assets/west6.jpg'
import west7 from '../../../../assets/west7.jpg'
import west8 from '../../../../assets/west8.jpg'
import west9 from '../../../../assets/west9.jpg'
import west10 from '../../../../assets/west10.jpg'



const Konten = () => {

    const [infoTop10, setInfoTop10] = useState([]);
    const [infoTren, setInfoTren] = useState([]);
    const [infoOnly, setInfoOnly] = useState([]);
    const [infoWest, setInfoWest] = useState([]);

    useEffect(() => {
        setInfoTop10([])
        setInfoTop10([{nomor:1,gambar:top1},
                        {nomor:2,gambar:top2},
                        {nomor:3,gambar:top3},
                        {nomor:4,gambar:top4},
                        {nomor:5,gambar:top5},
                        {nomor:6,gambar:top6},
                        {nomor:7,gambar:top7},
                        {nomor:8,gambar:top8},
                        {nomor:9,gambar:top9},
                        {nomor:10,gambar:top10}

        ])
    },[])

    useEffect(() => {
        setInfoTren([])
        setInfoTren([{gambar:tren1},
                        {gambar:tren2},
                        {gambar:tren3},
                        {gambar:tren4},
                        {gambar:tren5},
                        {gambar:tren6},
                        {gambar:tren7},
                        {gambar:tren8},
                        {gambar:tren9},
                        {gambar:tren10}

        ])
    },[])

    useEffect(() => {
        setInfoOnly([])
        setInfoOnly([{gambar:only1},
                        {gambar:only2},
                        {gambar:only3},
                        {gambar:only4},
                        {gambar:only5},
                        {gambar:only6},
                        {gambar:only7},
                        {gambar:only8},
                        {gambar:only9},
                        {gambar:only10}

        ])
    },[])

    useEffect(() => {
        setInfoWest([])
        setInfoWest([{gambar:west1},
                        {gambar:west2},
                        {gambar:west3},
                        {gambar:west4},
                        {gambar:west5},
                        {gambar:west6},
                        {gambar:west7},
                        {gambar:west8},
                        {gambar:west9},
                        {gambar:west10}

        ])
    },[])
  

    return (
        <Content>

            <div class="kontentengah">
                <div class="banner">
                    <div class="kontainerkonten">

                        <div class="kontainergenre">
                            <p class="genretag">Romance</p>
                            <span class="tag">&#8226;</span>
                            <p class="genretag">Friendship</p>
                            <span class="tag">&#8226;</span>
                            <p class="genretag">Surprise</p>
                            <span class="tag">&#8226;</span>
                            <p class="genretag">Love</p>
                        </div>


                        <div class="kontainertombol">
                            <div class="kontainermylist">
                                <FontAwesomeIcon class="icon" icon={faPlus} />
                                <p class="tulisanmylist">My List</p>
                            </div>
                            
                            <Link to="/browse/RachmaMovie" class="kontainerplay">
                                <div class="kontainertombolplay">
                                    <FontAwesomeIcon class="iconplay" icon={faPlay} />
                                    <p class="tulisanplay">Play</p>
                                </div>    
                            </Link>

                            <div class="kontainermylist">
                                <img class="icon" src={iconinfo} alt="icon info"></img>
                                <p class="tulisanmylist">Info</p>
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>

            <div class="kontainerlistfilm">

            <p class="judulkategori">Only on Netflix</p>
                <div class="top10">
                {infoOnly.map((post) => (
                <NormalCard
                gambar={post.gambar}
                />
                ))}
                </div>

                <p class="judulkategori2">Top 10 in Indonesia Today</p>
                <div class="top10">
                {infoTop10.map((post) => (
                <Top10Card
                angka={post.nomor}
                gambar={post.gambar}
                />
                ))}
                </div>

                <p class="judulkategori2">Trending Now</p>
                <div class="top10">
                {infoTren.map((post) => (
                <NormalCard
                gambar={post.gambar}
                />
                ))}
                </div>

                <p class="judulkategori2">Western TV Dramas</p>
                <div class="top10">
                {infoWest.map((post) => (
                <NormalCard
                gambar={post.gambar}
                />
                ))}
                </div>

                

                <div class="footer"/>
            </div>    
            

        </Content>
      );
};

export default Konten;
