import React, {useEffect} from 'react';
import '../../../index.css';
import { Content } from './style';
import Navbar from '../../../components/Navbar'
import NavbarMobile from '../../../components/NavbarMobile'
import Konten from './Konten'



const MovieList = () => {

    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])
    
    const [width, setWidth] = React.useState(window.innerWidth);
    const breakpoint = 450;
    React.useEffect(() => {
    const handleResizeWindow = () => setWidth(window.innerWidth);
        window.addEventListener("resize", handleResizeWindow);
        return () => {
        window.removeEventListener("resize", handleResizeWindow);
        };
    }, []);
    if (width > breakpoint) {
        return (
        <Content>
            <Navbar/>
            <Konten/>

        </Content>
        );
    }

    return (
        <Content>
            <NavbarMobile/>
            <Konten/>

        </Content>
      );
};

export default MovieList;
