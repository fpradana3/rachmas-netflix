import styled from '@emotion/styled';
import bannerrachma from '../../../assets/rachma.jpg'

export const Content = styled.div`
    @media screen {

        font-family: 'Netflix Sans','Helvetica Neue',Helvetica,Arial,sans-serif;

        background-color: #141414;
        height: 100%;
        min-height:150vh;
        
        .logonetflixcontainer{
            position:fixed;
            
            width:100%;
        }

        .logonetflix{
            margin-left:4vw;
            cursor:pointer;
            margin-top:2vh;
            height:calc(3vw + 2vh);
        }

        .kontentengah{
            width:100%;
            height:100vh;
        }

        .banner{
            min-height:100vh;
            background-repeat: no-repeat;
            background-image:linear-gradient(transparent 30%, #141414), url(${bannerrachma});
            background-position:center;
            background-size:cover;
        }

        @media (max-width: 1000px) {
            background-color:black;

            .banner{
                background-image:linear-gradient(transparent 30%, black), url(${bannerrachma});
            }
        }

    }

        
            
        `;
