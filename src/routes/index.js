import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { history } from './history';
import Home from './Home';
import lamanpertama from './Movie/MovieList';
import RachmaMovie from './Movie/RachmaMovie';
import "../index.css"

const Routes = () => (
  <Router history={history}>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/browse" component={lamanpertama} />
      <Route exact path="/browse/RachmaMovie" component={RachmaMovie} />
    </Switch>
  </Router>
);

export default Routes;
