import React, { useState, useEffect, useCallback } from 'react';
import '../../index.css';
import { Content } from './style';
import logonetflix from '../../assets/logonetflix.png'
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBell, faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons'
import logoprofil from '../../assets/logoprofile.png'



const Navbar = () => {
    const [y, setY] = useState(window.scrollY);

    const handleNavigation = useCallback(
        e => {
          const window = e.currentTarget;
          setY(window.scrollY);
          console.log(y);
          if(y<=50){
            var elem = document.getElementById("diubah");
            elem.style.transition = "background-color 0.5s";
            elem.style.backgroundColor = "transparent";
        }else{
            var elem2 = document.getElementById("diubah");
            elem2.style.transition = "background-color 0.5s";
            elem2.style.backgroundColor = "rgba(0, 0, 0, 0.8)";
            }
        }, [y]
      );
      
      useEffect(() => {
        setY(window.scrollY);
        window.addEventListener("scroll", handleNavigation);
      
        return () => {
          window.removeEventListener("scroll", handleNavigation);
        };
      }, [handleNavigation]);

    return (
        <Content>
            <div id="diubah" class="logonetflixcontainer">
                <Link to="/">
                    <img class="logonetflix" src={logonetflix} alt="logo netflix"></img>
                </Link>

                <div class="containertext">
                  <p class="current">Home</p>
                  <p class="texnav">Series</p>
                  <p class="texnav">Films</p>
                  <p class="texnav">New & Popular</p>
                  <p class="texnav">My List</p>
                  
                  
                </div>

                <div class="containerkanan">
                  <FontAwesomeIcon class="icon" icon={faMagnifyingGlass} />
                  <p class="namaprofil">Rachma</p>
                  <FontAwesomeIcon class="icon" icon={faBell} />
                  <div class="myprofile">
                  <img class="logoprofil" src={logoprofil} alt="logo profil"></img>
                  </div>
                </div>
              
            </div>

        </Content>
      );
};

export default Navbar;
