import styled from '@emotion/styled';


export const Content = styled.div`
    @media screen {
        z-index:5;

        font-family: 'Netflix Sans','Helvetica Neue',Helvetica,Arial,sans-serif;
        
        .logonetflixcontainer1{
            z-index:5;
            position:fixed;
            display:flex;
            width:100%;
            align-items:center;
            background-image: linear-gradient(#141414 5%, transparent);
            padding-top:1.6vh;
            padding-bottom:1.6vh;
        }

        .iconback{
            margin-left:4vw;
            color:white;
            cursor:pointer;
            margin-top:0.9vh;
            margin-bottom:0.4vh;
            height:calc(5px + 2vw);
        }

        .containertext{
            display: flex;
            color:whitesmoke;
            font-size:calc(4.5px + 0.7vw);
            margin-left:2vw;
        }

        .current{
            cursor: context-menu;
            margin-left:0.6vw;
            margin-right:0.6vw;
            font-weight:600;
        }

        .namaprofil{
            margin-left:1vw;
            margin-right:1vw;
            font-size: calc(4.5px + 0.7vw);
        }

        .texnav{
            cursor:pointer;
            margin-left:1vw;
            margin-right:1vw;
        }

        .icon{
            cursor:pointer;
            margin-left:1vw;
            margin-right:1vw;
            height: calc(5px + 1vw);
        }

        .containerkanan{
            margin-left:auto;
            margin-right:4vw;
            color:whitesmoke;
            font-size:calc(5px + 1vw);
            display:flex;
            height:100%;
            align-items:center;
        }

        .myprofile{
            height:100%;
            align-items:center;
            padding-top:0.5vh;
        }

        .logoprofil{
            cursor:pointer;
            margin-left:1vw;
            margin-right:1vw;
            height:calc(5px + 2vw);
            border-radius:4px;
        }


    }

        
            
        `;
