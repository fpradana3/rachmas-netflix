import React, { useState, useEffect, useCallback } from 'react';
import '../../index.css';
import { Content } from './style';
import logonetflix from '../../assets/logon.svg'
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSliders, faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons'
import logoprofil from '../../assets/logoprofile.png'



const NavbarMobile = () => {
    const [y, setY] = useState(window.scrollY);

    const handleNavigation = useCallback(
        e => {
          const window = e.currentTarget;
          setY(window.scrollY);
          console.log(y);
          if(y<=50){
            var elem = document.getElementById("diubah");
            elem.style.transition = "background-color 0.5s, transform 0.5s";
            elem.style.backgroundColor = "transparent";
            elem.style.transform = "translateY(0%)";
        }else{
            var elem2 = document.getElementById("diubah");
            elem2.style.transition = "background-color 0.5s, transform 0.5s";
            elem2.style.backgroundColor = "rgba(0, 0, 0, 0.8)";
            elem2.style.transform = "translateY(-60%)";
            }
        }, [y]
      );
      
      useEffect(() => {
        setY(window.scrollY);
        window.addEventListener("scroll", handleNavigation);
      
        return () => {
          window.removeEventListener("scroll", handleNavigation);
        };
      }, [handleNavigation]);

    return (
        <Content>
          <div id="diubah" class="kontainer">
            <div  class="logonetflixcontainer">
                <Link to="/">
                    <img class="logonetflix" src={logonetflix} alt="logo netflix"></img>
                </Link>

                <div class="containerkanan">
                  <FontAwesomeIcon class="icon" icon={faMagnifyingGlass} />
                  <FontAwesomeIcon class="icon" icon={faSliders} />
                  <div class="myprofile">
                  <img class="logoprofil" src={logoprofil} alt="logo profil"></img>
                  </div>
                </div>
              
            </div>

            <div class="kontainerbawah">
              <p class="texbawah">Series</p>
              <p class="texbawah">Films</p>
              <p class="texbawah">Categories</p>
            </div>


            

          </div>
            

        </Content>
      );
};

export default NavbarMobile;
