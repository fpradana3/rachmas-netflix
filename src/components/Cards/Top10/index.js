import React from 'react';
import '../../../index.css';
import { Content } from './style';

const Top10Card = ({
    gambar, angka
  }) => {

    return (
        <Content>
            <div class="kontainertop10">
                <p class="iconangka">{angka}</p>
                <img class="top" src={gambar} alt="top img"></img>
            </div>

        </Content>
    );


}

export default Top10Card;