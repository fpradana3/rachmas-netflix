import styled from '@emotion/styled';

export const Content = styled.div`
    @media screen {
        font-family: 'Netflix Sans','Helvetica Neue',Helvetica,Arial,sans-serif;
        height: 100%;


        .kontainertop10{
            display:flex;
            align-items:center;
            padding:0;
            width:32vh;
            height:31vh;
            cursor:pointer;
        }

        .iconangka{
            font-size:30vh;
            color:black;
            font-weight:700;
            stroke: rgb(89, 89, 89);
            stroke-width:2vh;
            -webkit-text-stroke: 1vh rgb(89, 89, 89);
            margin:0;
            transform:translateY(12%);
            z-index:2;
            letter-spacing:0;
        }

        .top{
            height:30vh;
            z-index:1;
            transform:translateX(-30%);
        }
    }

        
            
        `;
