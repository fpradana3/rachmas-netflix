import React from 'react';
import '../../../index.css';
import { Content } from './style';

const NormalCard = ({
    gambar
  }) => {

    return (
        <Content>
            <div class="kontainernormal">
                <img class="gambarnormal" src={gambar} alt="img normal"></img>
            </div>

        </Content>
    );


}

export default NormalCard;