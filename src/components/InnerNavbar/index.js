import React, { useState, useEffect, useCallback } from 'react';
import '../../index.css';
import { Content } from './style';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft,faSliders, faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons'
import logoprofil from '../../assets/logoprofile.png'



const InnerNavbar = () => {
    const [y, setY] = useState(window.scrollY);

    const handleNavigation = useCallback(
        e => {
          const window = e.currentTarget;
          setY(window.scrollY);
          console.log(y);
          if(y<=50){
            var elem = document.getElementById("diubah1");
            elem.style.transition = "background-color 0.5s";
            elem.style.backgroundColor = "transparent";
        }else{
            var elem2 = document.getElementById("diubah1");
            elem2.style.transition = "background-color 0.5s";
            elem2.style.backgroundColor = "rgba(0, 0, 0, 0.8)";
            }
        }, [y]
      );
      
      useEffect(() => {
        setY(window.scrollY);
        window.addEventListener("scroll", handleNavigation);
      
        return () => {
          window.removeEventListener("scroll", handleNavigation);
        };
      }, [handleNavigation]);

    return (
        <Content>
          <div id="diubah1" class="kontainer1">
            <div  class="logonetflixcontainer1">
                <Link to="/browse">
                    <FontAwesomeIcon class="iconback" icon={faArrowLeft} />
                </Link>

                <div class="containerkanan">
                  <FontAwesomeIcon class="icon" icon={faMagnifyingGlass} />
                  <FontAwesomeIcon class="icon" icon={faSliders} />
                  <div class="myprofile">
                  <img class="logoprofil" src={logoprofil} alt="logo profil"></img>
                  </div>
                </div>
              
            </div>

            

          </div>
            

        </Content>
      );
};

export default InnerNavbar;
