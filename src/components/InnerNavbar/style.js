import styled from '@emotion/styled';


export const Content = styled.div`
    @media screen {
        z-index:5;
        font-family: 'Netflix Sans','Helvetica Neue',Helvetica,Arial,sans-serif;

        .kontainer1{
            z-index:5;
            position:fixed;
            display:flex;
            flex-direction:column;
            width:100%;
            background-image: linear-gradient(#141414 5%, transparent);
        }

        .texbawah{
            cursor:pointer;
            flex:33.3%;
            text-align: center;
            margin-top:8px;
        }

        .kontainerbawah{
            width:90%;
            margin-left:auto;
            margin-right:auto;
            height:calc(3vw + 3vh);
            display: flex;
            justify-content: center;
            color:whitesmoke;
            font-size:calc(10px + 1vw);
        }
        
        .logonetflixcontainer1{
            position:relative;
            display:flex;
            width:100%;
            align-items:center;
            padding-top:1.1vh;
            padding-bottom:1.1vh;
        }

        .iconback{
            color:white;
            margin-left:4vw;
            cursor:pointer;
            margin-top:0.9vh;
            margin-bottom:0.4vh;
            height:calc(10px + 3vw);
        }

        .containertext{
            display: flex;
            color:whitesmoke;
            font-size:calc(4.5px + 0.7vw);
            margin-left:2vw;
        }

        .current{
            cursor: context-menu;
            margin-left:0.6vw;
            margin-right:0.6vw;
            font-weight:600;
        }

        .namaprofil{
            margin-left:1vw;
            margin-right:1vw;
            font-size: calc(4.5px + 0.7vw);
        }

        .texnav{
            cursor:pointer;
            margin-left:1vw;
            margin-right:1vw;
        }

        .icon{
            cursor:pointer;
            margin-left:3vw;
            margin-right:3vw;
            height: calc(10px + 3vw);
        }

        .containerkanan{
            margin-left:auto;
            margin-right:4vw;
            color:whitesmoke;
            font-size:calc(5px + 1vw);
            display:flex;
            height:100%;
            align-items:center;
        }

        .myprofile{
            height:100%;
            align-items:center;
            padding-top:0.5vh;
        }

        .logoprofil{
            cursor:pointer;
            margin-left:3vw;
            margin-right:3vw;
            height:calc(10px + 3vw);
            border-radius:4px;
        }


    }

        
            
        `;
