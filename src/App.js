import './App.css';
import Routes from './routes';
import 'animate.css'

function App() {
  return (
          <Routes />
  );
}

export default App;
